<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\TypeAlienRepository")
 */
class TypeAlien
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marker;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $speed;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Alien", mappedBy="typeAlien")
     */
    private $aliens;

    public function __construct()
    {
        $this->aliens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarker(): ?string
    {
        return $this->marker;
    }

    public function setMarker(?string $marker): self
    {
        $this->marker = $marker;

        return $this;
    }

    public function getSpeed(): ?string
    {
        return $this->speed;
    }

    public function setSpeed(?string $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return Collection|Alien[]
     */
    public function getAliens(): Collection
    {
        return $this->aliens;
    }

    public function addAlien(Alien $alien): self
    {
        if (!$this->aliens->contains($alien)) {
            $this->aliens[] = $alien;
            $alien->setTypeAlien($this);
        }

        return $this;
    }

    public function removeAlien(Alien $alien): self
    {
        if ($this->aliens->contains($alien)) {
            $this->aliens->removeElement($alien);
            // set the owning side to null (unless already changed)
            if ($alien->getTypeAlien() === $this) {
                $alien->setTypeAlien(null);
            }
        }

        return $this;
    }
}
