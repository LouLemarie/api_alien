<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\AlienRepository")
 */
class Alien
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="datetime")
     */
    private $arrival_date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $destination_latitude;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $destination_longitude;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeAlien", inversedBy="aliens")
     */
    private $typeAlien;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getArrivalDate(): ?\DateTimeInterface
    {
        return $this->arrival_date;
    }

    public function setArrivalDate(\DateTimeInterface $arrival_date): self
    {
        $this->arrival_date = $arrival_date;

        return $this;
    }

    public function getDestinationLatitude(): ?string
    {
        return $this->destination_latitude;
    }

    public function setDestinationLatitude(?string $destination_latitude): self
    {
        $this->destination_latitude = $destination_latitude;

        return $this;
    }

    public function getDestinationLongitude(): ?string
    {
        return $this->destination_longitude;
    }

    public function setDestinationLongitude(?string $destination_longitude): self
    {
        $this->destination_longitude = $destination_longitude;

        return $this;
    }

    public function getTypeAlien(): ?TypeAlien
    {
        return $this->typeAlien;
    }

    public function setTypeAlien(?TypeAlien $typeAlien): self
    {
        $this->typeAlien = $typeAlien;

        return $this;
    }
}
