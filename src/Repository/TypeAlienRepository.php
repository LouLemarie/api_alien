<?php

namespace App\Repository;

use App\Entity\TypeAlien;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeAlien|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeAlien|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeAlien[]    findAll()
 * @method TypeAlien[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeAlienRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeAlien::class);
    }

    // /**
    //  * @return TypeAlien[] Returns an array of TypeAlien objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeAlien
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
